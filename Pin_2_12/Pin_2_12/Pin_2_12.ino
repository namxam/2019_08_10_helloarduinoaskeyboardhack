/*
  Keyboard test

  For the Arduino Leonardo, Micro or Due

  Reads a byte from the serial port, sends a keystroke back.
  The sent keystroke is one higher than what's received, e.g. if you send a,
  you get b, send A you get B, and so forth.

  The circuit:
  - none

  created 21 Oct 2011
  modified 27 Mar 2012
  by Tom Igoe

  This example code is in the public domain.

  http://www.arduino.cc/en/Tutorial/KeyboardSerial
*/

#include "Keyboard.h"


 struct PinState {
    public :
    int pin;
    bool previousValue;
    bool isPressing;
    long lastTime;
    private:
    
  };
long frame=0;
int cooldowninMilli=400;

PinState trackedPins []  = {
  { 2 , false , false },
  { 3 , false , false },
  { 4 , false , false },
  { 5 , false , false },
  { 6 , false , false },
  { 7 , false , false },
  { 8 , false , false },
  { 9 , false , false },
  { 10 , false , false },
  { 11 , false , false },
  { 12 , false , false }
  };

void setup() {
  
  Serial.begin(9600);
  
  Serial.println("Hello World");
  Keyboard.print("Hello World");  
  int pinsCount =10;// sizeof(trackedPins);
  for (int i = 0; i <= pinsCount ; i++){
    pinMode (trackedPins[i].pin , INPUT_PULLUP);
  }
}
void loop() { 

  int pinsCount = sizeof(trackedPins);
  delay(100);
  String msg;
  msg = "Frame:";
  msg.concat(frame);
  Serial.println(msg );  
  /*
  for (int i = 0 ; i < pinsCount ; i++){
    if( digitalRead (trackedPins[i].pin == LOW)){
      msg = "Pin:";
      msg.concat(i);
      Serial.println(msg);  
      //Keyboard.println(msg);
    }
    else {
      
      Serial.println("None"); 
      }
  }
  //*/
  frame++;
}

bool HasCooldown(long giveTime){
 long now = millis();
 long diff =now-giveTime;
 return diff< cooldowninMilli && diff>=0; 
 }

bool IsDetectedChange(PinState & state){
  bool newValue = digitalRead (state.pin) == LOW;
  bool hasChanged = (newValue != state.previousValue );
  state.previousValue = newValue;
  state.isPressing = newValue;
  return hasChanged;
}
