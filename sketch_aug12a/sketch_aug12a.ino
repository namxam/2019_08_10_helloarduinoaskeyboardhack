/*
  Keyboard test

  For the Arduino Leonardo, Micro or Due

  Reads a byte from the serial port, sends a keystroke back.
  The sent keystroke is one higher than what's received, e.g. if you send a,
  you get b, send A you get B, and so forth.

  The circuit:
  - none

  created 21 Oct 2011
  modified 27 Mar 2012
  by Tom Igoe

  This example code is in the public domain.

  http://www.arduino.cc/en/Tutorial/KeyboardSerial
*/

#include "Keyboard.h"

void DoAtStart(){
 // OpenCmd_Desktop();
 // DownloadUrlLinkInFolder("https://avatars0.githubusercontent.com/u/20149493");
 // Rename("20149493", "EloiWasHere.png");
 // ExitCmd();
}

void ButtonActiveAtStart0( ){

  CreateUrlFileOnDesktop("AdminRight.url","https://www.youtube.com/watch?v=dQw4w9WgXcQ");
  }
void ButtonActiveAtStart1( ){

  }
void ButtonActiveAtStart2( ){}
void ButtonActiveAtStart3( ){WriteText("33333333");}
void ButtonActiveAtStart4( ){WriteText("44444444");}
void ButtonActiveAtStart5( ){WriteText("55555555");}
void ButtonActiveAtStart6( ){WriteText("66666666");}
void ButtonActiveAtStart7( ){WriteText("77777777");}
void ButtonActiveAtStart8( ){WriteText("Hey Mon Ami");}
void ButtonActiveAtStart9( ){WriteText("Hey Mon Ami");}
void ButtonActiveAtStart10( ){WriteText("12345678");}

void ButtonChange0(bool value){if(value){
  OpenCmd_Music();
  DownloadUrlLinkInFolderAndOpen("https://ia800504.us.archive.org/33/items/TetrisThemeMusic/Tetris.ogg","Tetris.ogg");
  ExitCmd();
  }}
  
void ButtonChange1(bool value){if(value){
  OpenCmd_Images();
  DownloadUrlLinkInFolderAndOpen("https://gitlab.com/eloistree/2019_08_10_helloarduinoaskeyboardhack/raw/master/Images/HelpedOrBroke.png","HelpedOrBroke.png");
  ExitCmd();
  }}
void ButtonChange2(bool value){if(value)WriteText("c22222222");}
void ButtonChange3(bool value){if(value)WriteText("c33333333");}
void ButtonChange4(bool value){if(value){
  OpenUrlToFullScreen("https://www.youtube.com/watch?v=1ER67r8OCW8&feature=youtu.be&t=27", true, true);
  }}
void ButtonChange5(bool value){if(value){
  OpenUrlToFullScreen("https://www.youtube.com/watch?v=1ER67r8OCW8&feature=youtu.be&t=27", false, false);
  }}
void ButtonChange6(bool value){if(value)WriteText("c66666666");}
void ButtonChange7(bool value){if(value) {
  OpenCurrentFolder();
  PullPushWithGit();
  }
}
void ButtonChange8(bool value){if(value) {
  OpenCurrentFolder();
  AddAndCommitWithGit();
  }
}

void ButtonChange9(bool value){if(value)SwitchKeyboardLayout();}
void ButtonChange10(bool value){if(value)WriteText("5645");}


bool displayFrame=false;
long delayBetweenFrame=100;
//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
// DONT GO HERE EXCEPT TO CHANGE PINS OR IF YOU KNOW WHAT YOU DOING  //
//////////////////////////////////////////////////////////////////////


 struct PinState {
    public :
    int pin;
    bool previousValue;
    long lastTime;
    long cooldownInMilli;
    
    bool IsPinOn(){
       return digitalRead (pin) == LOW;
    }
    void SetManuallyAs(bool value){
      previousValue=value;
      lastTime=millis();
      }
    bool IsLastStateOn(){
       return previousValue;
    }
    bool HasChanged(bool refreshIfTrue){ 

      if(HasCooldown(previousValue)){
       return false; 
      }
      
      bool isOn = IsPinOn();
      bool isNew = isOn != previousValue;

      if( isNew && refreshIfTrue){
        SetAsChanged(isOn);
      }
      return isNew;
    }
    bool HasCooldown(long giveTime){
       long now = millis();
       long diff =now-giveTime;
       return diff< cooldownInMilli && diff>=0; 
    }

    bool SetAsChanged(bool newValue){
      lastTime = millis();
      previousValue=newValue;
    }
    
    private:
    
  };

//////////////////////////////////////////////////////////////////////////////
//**//Change the value here based on how you connect buttons on pin 2-12//**//
//////////////////////////////////////////////////////////////////////////////
PinState trackedPins []  = {
  { 2 , false,0,200  },
  { 5 , false,0,200  },
  { 6 , false ,0,200 },
  { 7 , false ,0,200 },
  { 4 , false,0,200  },
  { 9 , false ,0,200 },
  { 8 , false ,0,200 },
  { 3 , false ,0,200 },
  { 11 , false ,0,200 },
  { 10 , false ,0,200 },
  { 12 , false ,0,200 }
  };

long frame=0;
int pinsCount = 11;
void setup() {
  pinsCount = 11;
  Serial.begin(9600);
  
  //pinsCount= sizeof(trackedPins);
  for(int y=0; y < pinsCount ; y++){
    pinMode (trackedPins[y].pin , INPUT_PULLUP);
  }

}
void loop() { 

if(frame==0){
  delay(1000); 
  DoAtStart();
  for (int y=0; y<pinsCount; y++){
    if( IsOn(y)){
         ButtonActiveAtStart(y);
         trackedPins[y].SetManuallyAs(true);
      }
  }
}
//*/
  delay(delayBetweenFrame); 
  String msg;
  
  if(displayFrame){
    msg = "Frame:";
    msg.concat(frame);
    Serial.println(msg );  
  }
  ///*
  bool changeDetected=false;
  for (int y=0; y<pinsCount; y++){
    if(trackedPins[y].HasChanged(true)){
      msg = "Pin: ";
      msg.concat(y);
      msg.concat(" ");
      msg.concat(trackedPins[y].pin);
      Serial.println(msg);  
      ButtonChange(y,trackedPins[y].IsLastStateOn());
      //Keyboard.println(msg);
      
    }
    else { 
    //  Serial.println("None"); 
      }
  }
  //*/
  frame++;
}

bool IsOn(int index){
  return digitalRead (trackedPins[index].pin) == LOW;
  }




// Those instruction will be call only if the button is pressed when the Arduino start
void ButtonActiveAtStart(int index){
switch(index){
  case 0:ButtonActiveAtStart0(); break;
  case 1:ButtonActiveAtStart1(); break;
  case 2:ButtonActiveAtStart2(); break;
  case 3:ButtonActiveAtStart3(); break;
  case 4:ButtonActiveAtStart4(); break;
  case 5:ButtonActiveAtStart5(); break;
  case 6:ButtonActiveAtStart6(); break;
  case 7:ButtonActiveAtStart7(); break;
  case 8:ButtonActiveAtStart8(); break;
  case 9:ButtonActiveAtStart9(); break;
  case 10:ButtonActiveAtStart10(); break;
  }  
}


// Those instruction will be call each time a change is detected on the 11 buttons tracked.
void ButtonChange(int index, bool value){
switch(index){
  case 0:ButtonChange0(value); break;
  case 1:ButtonChange1(value); break;
  case 2:ButtonChange2(value); break;
  case 3:ButtonChange3(value); break;
  case 4:ButtonChange4(value); break;
  case 5:ButtonChange5(value); break; 
  case 6:ButtonChange6(value); break;
  case 7:ButtonChange7(value); break;
  case 8:ButtonChange8(value); break;
  case 9:ButtonChange9(value); break;
  case 10:ButtonChange10(value); break;
  }  
}


void WriteText( String text){
  
    Serial.println(text);
    Keyboard.println(text);  
}


///////////////////////////////////////////
////// Cool Window Stuff  /////////////////
///////////////////////////////////////////

void OpenCurrentFolder(){
 Keyboard.println("cmd");
 delay(500);
}

void AddAndCommitWithGit(){

 Keyboard.println("git status");
 delay(10);
 Keyboard.println("git add .");
 delay(2000);
 Keyboard.println("git commit -m \"Auto-commit\"");
}
void PullPushWithGit(){
  AddAndCommitWithGit();
 delay(10);
 Keyboard.println("git pull");
  AddAndCommitWithGit();
 delay(10);
 Keyboard.println("git push");
  
}

void CheckCurrentStateOfProjectInSelectedFolder(){
  OpenCurrentFolder();
 delay(10);
 Keyboard.println("git status");
}

void SwitchKeyboardLayout(){
  // NEW WINDOW 10
  // Window + Space change keyboard
  Keyboard.press(KEY_LEFT_GUI);
  Keyboard.press(' ');
  Keyboard.releaseAll();
  // OLD WINDOW 10
  // Left Alt + Shift Change Keyboard
  Keyboard.press(KEY_LEFT_ALT);
  Keyboard.press(KEY_LEFT_SHIFT);
  Keyboard.releaseAll();
}

void OpenCmdWindow(){
    Keyboard.releaseAll();
    Keyboard.press(KEY_LEFT_GUI);
    Keyboard.press('r');
    Keyboard.releaseAll();
    delay(100);
    Keyboard.println("cmd");
    delay(100);
  }void ExitCmd(){
  Keyboard.println("exit");
  }

  void OpenCmdWindowInDesktop(int delayBetween=250){
     Keyboard.releaseAll();
    Keyboard.press(KEY_LEFT_GUI);
    Keyboard.press('r');
    Keyboard.releaseAll();
    delay(delayBetween);
    Keyboard.println("cmd"); 
    delay(delayBetween);
    Keyboard.println("cd Desktop");
    Keyboard.println("cd OneDrive/Desktop");
    }

    void CreateUrlFileOnDesktop(String fileName, String link){

    OpenCmdWindowInDesktop(250);
    
    Keyboard.println("del "+fileName+".url"); 
    Keyboard.println("echo [InternetShortcut] > "+fileName+".url");
    Keyboard.println("echo URL="+link+" >> "+fileName+".url");
    Keyboard.println("Exit");
  
  }

  void SetWindowToMaxVolume(){
    
    delay(100);
  Keyboard.println("sndvol -f 1111111"); 
  Keyboard.println("Exit"); 
  delay(300);
   for (int i=0; i<100; i++){
    Keyboard.press(KEY_UP_ARROW);
    
    Keyboard.release(KEY_UP_ARROW);
  }
  delay(600);
 
 }

 void OpenUrlToFullScreen(String url, bool soundMax , bool fullScreen){
 
  if(soundMax)
  {
    OpenCmdWindow();
   SetWindowToMaxVolume();
  }
  String msg ="start ";
  msg.concat(url);
   OpenCmdWindow();
    Keyboard.println(msg); 
  //Keyboard.println("Exit"); 
  delay(1000);
  
  if(fullScreen){
    Keyboard.press(KEY_F11);
    Keyboard.release(KEY_F11);
    delay(1000);
    Keyboard.print("f");
  }
  if(soundMax){
  for (int i=0; i<20; i++){
      delay(50);
        Keyboard.press(KEY_UP_ARROW);
      delay(50);
        Keyboard.release(KEY_UP_ARROW);
      }
   }
  
  }

void OpenCmd_Desktop(){
  OpenCmdWindowInDesktop(250);
  }
  void OpenCmd_Images(){
 
    OpenCmdWindowInDesktop(250);
    Keyboard.println("cd ../Images");
    Keyboard.println("cd ../../Images");
}
void OpenCmd_Video(){

    OpenCmdWindowInDesktop(250);
    Keyboard.println("cd ../Videos");
    Keyboard.println("cd ../../Videos");

}
void OpenCmd_Music(){
    OpenCmdWindowInDesktop(250);
    Keyboard.println("cd ../Music");
    Keyboard.println("cd ../../Music");
}
void OpenCmd_Downloads(){
    OpenCmdWindowInDesktop(250);
    Keyboard.println("cd ../Downloads");
    Keyboard.println("cd ../../Downloads");
}
void OpenCmd_Documents(){
 
    OpenCmdWindowInDesktop(250);
    Keyboard.println("cd ../Documents");
    Keyboard.println("cd ../../Documents");
}

void DownloadUrlLinkInFolder(String url){
  String msg = "curl -O ";
  msg.concat(url);
    Keyboard.println(msg);
}

void OpenCurrentDirectory(){
    Keyboard.println("start .");
  }

void DownloadUrlLinkInFolderAndOpen(String url, String fileNameWithExtension){
  String msg = "curl -O ";
  msg.concat(url);
  Keyboard.println(msg);
  msg = "start ";
  msg.concat(fileNameWithExtension);
  Keyboard.println(msg);

}

void Rename(String currentNameExt, String newNameExt){
   String msg = "rename ";
  msg.concat(currentNameExt);
  msg.concat(" ");
  msg.concat(newNameExt);
  Keyboard.println(msg);
  
  }
