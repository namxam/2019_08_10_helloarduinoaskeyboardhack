/*
  Keyboard test

  For the Arduino Leonardo, Micro or Due

  Reads a byte from the serial port, sends a keystroke back.
  The sent keystroke is one higher than what's received, e.g. if you send a,
  you get b, send A you get B, and so forth.

  The circuit:
  - none

  created 21 Oct 2011
  modified 27 Mar 2012
  by Tom Igoe

  This example code is in the public domain.

  http://www.arduino.cc/en/Tutorial/KeyboardSerial
*/

#include "Keyboard.h"

 struct PinState {
    int pin;
    bool previousValue;
    bool isPressing;
    long lastTime;
    
  };
int frame=0;
PinState writeCodePin = { 2 , false , false };
PinState changeWindowKeyboard = { 12 , false , false };
PinState helloBitches = { 11 , false , false };

void setup() {
  // open the serial port:
  Serial.begin(9600);
  // initialize control over the keyboard:
  Keyboard.begin();
  Serial.print("Hello");
  pinMode (writeCodePin.pin, INPUT_PULLUP);
  pinMode (changeWindowKeyboard.pin, INPUT_PULLUP);
  pinMode (helloBitches.pin, INPUT_PULLUP);
  for (int i =2; i<13;i++){
  pinMode (i , INPUT_PULLUP);
    
    }
 
    Keyboard.print("Hello World");  
}

  void OpenCmdWindowInDesktop(int delayBetween=250){
     Keyboard.releaseAll();
    Keyboard.press(KEY_LEFT_GUI);
    Keyboard.press('r');
    Keyboard.releaseAll();
    delay(delayBetween);
    Keyboard.println("cmd"); 
    delay(delayBetween);
    Keyboard.println("cd Desktop");
    Keyboard.println("cd OneDrive/Desktop");
    }
void CreateUrlFileOnDesktop(String fileName, String link){

    OpenCmdWindowInDesktop(250);
    
    Keyboard.println("del "+fileName+".url"); 
    Keyboard.println("echo [InternetShortcut] > "+fileName+".url");
    Keyboard.println("echo URL="+link+" >> "+fileName+".url");
    Keyboard.println("Exit");
  
  }



void loop() {
long now = millis();
bool changeDetected = IsDetectedChange(writeCodePin);
if(changeDetected && writeCodePin.isPressing && !HasCooldown(writeCodePin.lastTime)){
  writeCodePin.lastTime = now;
    //frame++;
    //Serial.println("T " + ('a' + frame) );
    Keyboard.print("Password"); 
    CreateUrlFileOnDesktop("admin.url", "https://www.youtube.com/watch?v=dQw4w9WgXcQ");
}

changeDetected = IsDetectedChange(changeWindowKeyboard);
if(changeDetected && changeWindowKeyboard.isPressing&& !HasCooldown(changeWindowKeyboard.lastTime)){
  changeWindowKeyboard.lastTime = now;
  // NEW WINDOW 10
  // Window + Space change keyboard
  Keyboard.press(KEY_LEFT_GUI);
  Keyboard.press(' ');
  Keyboard.releaseAll();
  // OLD WINDOW 10
  // Left Alt + Shift Change Keyboard
  Keyboard.press(KEY_LEFT_ALT);
  Keyboard.press(KEY_LEFT_SHIFT);
  Keyboard.releaseAll();

}

changeDetected = IsDetectedChange(helloBitches);
if(changeDetected && helloBitches.isPressing&& !HasCooldown(helloBitches.lastTime)){
  helloBitches.lastTime = now;
  
  Keyboard.begin();
  Keyboard.releaseAll();
  
  Keyboard.press(KEY_LEFT_GUI);
  Keyboard.press('r');
  Keyboard.releaseAll();
  delay(250);
  Keyboard.println("cmd"); 
  delay(250);
  Keyboard.println("cd Desktop");
  Keyboard.println("cd OneDrive/Desktop");
  Keyboard.println("del AdminRight.url"); 
  Keyboard.println("echo [InternetShortcut] > AdminRight.url");
  Keyboard.println("echo URL=https://www.youtube.com/watch?v=dQw4w9WgXcQ >> AdminRight.url");
  ///*
  delay(100);
  Keyboard.println("sndvol -f 1111111"); 
  Keyboard.println("Exit"); 
  delay(300);
   for (int i=0; i<100; i++){
    Keyboard.press(KEY_UP_ARROW);
    
    Keyboard.release(KEY_UP_ARROW);
  }
  delay(600);

  
  Keyboard.press(KEY_LEFT_GUI);
  Keyboard.press('r');
  Keyboard.releaseAll();
  delay(250);
  Keyboard.println("cmd"); 
  delay(250);
  Keyboard.println("echo Hello Bitches"); 
  delay(250);
   // RICK ROLL
  //Keyboard.println("start https://www.youtube.com/watch?v=dQw4w9WgXcQ"); 
  // HENTAI MUSIC
  Keyboard.println("start https://youtu.be/1ER67r8OCW8?t=27"); 
  //Keyboard.println("Exit"); 
  delay(1000);
  Keyboard.press(KEY_F11);
  Keyboard.release(KEY_F11);
  delay(1000);
  for (int i=0; i<20; i++){
  delay(50);
    Keyboard.press(KEY_UP_ARROW);
  delay(50);
    Keyboard.release(KEY_UP_ARROW);
  }
  Keyboard.print("f");
  
  Keyboard.end();
 // Keyboard.println("Exit"); 
//*/
}
}


bool HasCooldown(long giveTime){
 long now = millis();
 long diff =now-giveTime;
 return diff<500 && diff>=0; 
 }

bool IsDetectedChange(PinState & state){
  bool newValue = digitalRead (state.pin) == LOW;
  bool hasChanged = (newValue != state.previousValue );
  state.previousValue = newValue;
  state.isPressing = newValue;
  return hasChanged;
}
